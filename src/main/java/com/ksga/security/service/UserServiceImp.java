package com.ksga.security.service;

import com.ksga.security.dto.UserDto;
import com.ksga.security.model.User;
import com.ksga.security.repository.UserRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService{

private UserRepository userRepository;
private BCryptPasswordEncoder encoder;
@Autowired
  public void setEncoder(BCryptPasswordEncoder encoder) {
    this.encoder = encoder;
  }

  @Autowired
  public UserServiceImp(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = userRepository.findByEmail(email);
    if(user==null) throw new UsernameNotFoundException("USER NOT FOUND");
    return new CustomUserDetails(user);
  }

  //TODO: inserting
//  1. Insert user through email,password
//      2. inser user`s roles through userid roleid (default role=user)

  @Override
  public User insertNewUser(UserDto userDto) {
  userDto.setPassword(encoder.encode(userDto.getPassword()));
  userRepository.insertUser(userDto);
  User user = userRepository.findByEmail(userDto.getEmail());
  userRepository.insertRole(user.getId(),1);
    return user;
  }
}
