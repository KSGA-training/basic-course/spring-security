package com.ksga.security.service;

import com.ksga.security.dto.UserDto;
import com.ksga.security.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

  User  insertNewUser(UserDto userDto);

}
