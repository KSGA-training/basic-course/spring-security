package com.ksga.security.repository;

import com.ksga.security.dto.UserDto;
import com.ksga.security.model.Role;
import com.ksga.security.model.User;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {
@Select("select * from users where users.email=#{email}")
@Results(
    value = {
        @Result(property = "id",column = "id"),
        @Result(property = "roles",column = "id", many = @Many(select = "findRoleByUserId"))
    }
)
  public User findByEmail(@Param("email") String email);

@Select("select r.id,r.name from roles r inner join users_roles ur on r.id = ur.role_id where user_id=#{id}")
public List<Role> findRoleByUserId(@Param("id") int id);
@Insert(" insert into users (email, password) values (#{u.email},#{u.password})")
boolean insertUser(@Param("u")UserDto userDto);
@Insert("insert into users_roles(user_id, role_id) VALUES (#{userId},#{roleId})")
boolean insertRole(@Param("userId")int userId,@Param("roleId")int roleId);

}
