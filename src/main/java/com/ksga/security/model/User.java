package com.ksga.security.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
  private int id;
  private String  uid;
  private String email;
  private String password;
  List<Role> roles ;

}
