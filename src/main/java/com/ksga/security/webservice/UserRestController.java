package com.ksga.security.webservice;

import com.ksga.security.dto.UserDto;
import com.ksga.security.model.User;
import com.ksga.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRestController {
  private UserService userService;
@Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @PostMapping("/register")
  public User registerUser(@RequestBody UserDto userDto){
    return userService.insertNewUser(userDto);
  }

}
