package com.ksga.security.webservice;

import com.ksga.security.dto.UserDto;
import com.ksga.security.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRestController {

  @GetMapping("/secured")
  public String secured(){
    return "This is secured route";
  }
  @GetMapping("/unsecured")
  public String unsecured(){
    return "This is unsecured route";
  }

  @GetMapping("/test-secured")
  @Secured("ROLE_ADMIN")
  public String securedMethodLevel(){
    return "This method works with @Secure";
  }
  @GetMapping("/pre-auth")
  @PreAuthorize("#email==authentication.name")
  public String testPreAuth(String email){
    return "Hello "+email;
  }



}
