package com.ksga.security.configuration;

import com.ksga.security.jwt.JwtAuthenticationEntryPoint;
import com.ksga.security.jwt.JwtRequestFilter;
import com.ksga.security.service.UserService;
import com.ksga.security.service.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.authentication.AuthenticationManagerFactoryBean;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {

  @Bean
  public BCryptPasswordEncoder encoder(){
    return new BCryptPasswordEncoder();
  }
  @Autowired
private UserService userService;
  @Autowired
  private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
  @Autowired
  private JwtRequestFilter jwtRequestFilter;


//TODO: Define user here
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//   auth.inMemoryAuthentication()
////       .withUser("Dayan").password("{noop}1234").roles("ADMIN")
////       .and()
////       .withUser("Chanry").password("{noop}qwer").roles("USER");


    auth.userDetailsService(userService).passwordEncoder(encoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/register","/authenticate").permitAll()
        .antMatchers("/unsecured").hasAnyRole("ADMIN")
        .anyRequest().authenticated()
        .and()
//        .httpBasic();
    // make sure we use stateless session; session won't be used to
// store user's state.
    .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
// Add a filter to validate the tokens with every request
    http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
  }




  @Override
  public void configure(WebSecurity web) throws Exception {
    super.configure(web);
  }
  @Bean
  public AuthenticationManagerFactoryBean authenticationManagerFactoryBean(){
    return new AuthenticationManagerFactoryBean();
  }
}
